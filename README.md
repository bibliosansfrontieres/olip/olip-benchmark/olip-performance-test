# OLIP Performance Test

## Run performance test

```shell
npm run build && npm run start
```

## Configuration

| Variable                     | Example                                          | Default                                | Required | Description                                       |
| ---------------------------- | ------------------------------------------------ | -------------------------------------- | -------- | ------------------------------------------------- |
| `SCENARIO`                   | `1`                                              | `1`                                    | no       | Scenario number to run                            |
| `BOTS_TO_RUN`                | `5`                                              | `1`                                    | no       | Number of parallel performance test run           |
| `TEST_DURATION_IN_MINUTES`   | `5`                                              | `1`                                    | no       | Time of scenario duration (loop)                  |
| `HEADLESS_BROWSER`           | `true`                                           | `true`                                 | no       | Does web browser will run headless or not         |
| `CHROME_EXECUTABLE_PATH`     | `/usr/bin/google-chrome`                         |                                        | yes      | Path of chrome executable                         |
| `WINDOW_WIDTH`               | `1920`                                           | `1920`                                 | no       | Web browser window width                          |
| `WINDOW_HEIGHT`              | `1080`                                           | `1080`                                 | no       | Web browser window height                         |
| `SLEEP_BETWEEN_LOOPS`        | `1000`                                           | `1000`                                 | no       | Sleep between each scenario loop (ms)             |
| `OLIP_V1_URL`                | `http://olip.bibliosansfrontieres.org`           | `http://olip.bibliosansfrontieres.org` | no       | OLIP v1 URL used for OLIP v1 tests                |
| `OLIP_V2_URL`                | `https://fasld.staging.maestro.bsf-intranet.org` | `https://api.olip-v2.bsf-intranet.org` | no       | OLIP v2 URL used for OLIP v2 tests (can be a VM)  |
| `SLEEP_BETWEEN_ACTIONS`      | `1000`                                           | `1000`                                 | no       | Sleep between each action in a scenario (ms)      |
| `SLEEP_WATCHING_VIDEO`       | `5000`                                           | `5000`                                 | no       | Sleep while watching a video (ms)                 |
| `OLIP_V1_PLAYLIST_SELECTION` | `2,3,4,6,7,8,9,10`                               |                                        | no       | Playlist order to click on in an OLIP v1 scenario |

## Scenario 3 configuration

| Variable                 | Example                            | Default | Required | Description                                         |
| ------------------------ | ---------------------------------- | ------- | -------- | --------------------------------------------------- |
| `SCENARIO_3_OLIP_V2_URL` | `https://olip-v2.bsf-intranet.org` |         | yes      | OLIP v2 URL to use in scenario                      |
| `SCENARIO_3_THEME_IDS`   | `1;2`                              |         | yes      | Themes Ids that you want to test separated with `;` |
| `SCENARIO_3_USERNAME`    | `admin`                            |         | yes      | Username to log in with on OLIP                     |
| `SCENARIO_3_PASSWORD`    | `admin`                            |         | yes      | Password to log in with on OLIP                     |
