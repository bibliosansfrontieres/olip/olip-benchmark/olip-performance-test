import * as dotenv from 'dotenv'
dotenv.config()

export class Config {
  olipV1Url: string
  olipV2Url: string
  headlessBrowser: boolean
  windowWidth: number
  windowHeight: number
  sleepBetweenLoops: number
  sleepBetweenActions: number
  sleepWatchingVideo: number
  olipV1PlaylistSelection: string[]
  chromeExecutablePath: string
  botsToRun: number
  testDurationInMinutes: number
  scenario: string
  defaultTimeout: number

  constructor() {
    this.olipV1Url =
      process.env.OLIP_V1_URL ?? 'http://olip.bibliosansfrontieres.org'
    this.olipV2Url =
      process.env.OLIP_V2_URL ?? 'https://api.olip-v2.bsf-intranet.org'
    this.headlessBrowser = process.env.HEADLESS_BROWSER === 'true'
    this.windowWidth = parseInt(process.env.WINDOW_WIDTH ?? '1920')
    this.windowHeight = parseInt(process.env.WINDOW_HEIGHT ?? '1080')
    this.sleepBetweenActions = parseInt(
      process.env.SLEEP_BETWEEN_ACTIONS ?? '1000'
    )
    this.sleepBetweenLoops = parseInt(process.env.SLEEP_BETWEEN_LOOPS ?? '1000')
    this.olipV1PlaylistSelection = (
      process.env.OLIP_V1_PLAYLIST_SELECTION ?? ''
    ).split(',')
    if (process.env.CHROME_EXECUTABLE_PATH !== undefined) {
      this.chromeExecutablePath = process.env.CHROME_EXECUTABLE_PATH
    } else {
      throw new Error('Missing CHROME_EXECUTABLE_PATH')
    }
    this.sleepWatchingVideo = parseInt(
      process.env.SLEEP_WATCHING_VIDEO ?? '5000'
    )
    this.botsToRun = parseInt(process.env.BOTS_TO_RUN ?? '1')
    this.testDurationInMinutes = parseInt(
      process.env.TEST_DURATION_IN_MINUTES ?? '1'
    )
    this.scenario = process.env.SCENARIO ?? '1'

    this.defaultTimeout = parseInt(process.env.DEFAULT_TIMEOUT ?? '120000')
  }
}
