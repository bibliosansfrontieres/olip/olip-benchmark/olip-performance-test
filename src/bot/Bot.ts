import puppeteer, { type Browser } from 'puppeteer'
import { type IScenario } from '../scenarios'
import { join } from 'path'
import { type Config } from '../config'
import { sleep } from '../utils/sleep.util'

export class Bot {
  readonly identifier: number
  readonly config: Config
  readonly logsFilename: string
  browser: Browser
  scenario: IScenario
  loops: number
  errors: number

  constructor(config: Config, identifier: number, logsFilename: string) {
    this.identifier = identifier
    this.config = config
    this.logsFilename = logsFilename
    this.loops = 0
    this.errors = 0
  }

  async init(scenario: IScenario): Promise<void> {
    this.scenario = scenario
    const headless = this.config.headlessBrowser ? 'new' : false
    this.browser = await puppeteer.launch({
      headless,
      executablePath: this.config.chromeExecutablePath,
      args: [
        `--window-size=${this.config.windowWidth},${this.config.windowHeight}`,
        '--no-sandbox'
      ],
      defaultViewport: {
        width: this.config.windowWidth,
        height: this.config.windowHeight
      }
    })
  }

  async run(): Promise<void> {
    this.loops++
    const fileToImport = join(
      __dirname,
      '../scenarios',
      `${this.scenario.filename}.js`
    )
    const file = require(fileToImport)
    const scenario = new file[this.scenario.className](this)
    try {
      await scenario.init()
      await scenario.run()
      await sleep(this.config.sleepBetweenLoops)
      await this.browser.close()
      await this.init(this.scenario)
      await this.run()
    } catch (e) {
      console.error(e)
      this.errors++
      await sleep(this.config.sleepBetweenLoops)
      await this.browser.close()
      await this.init(this.scenario)
      await this.run()
      console.log('ERRRREEEUR')
    }
  }
}
