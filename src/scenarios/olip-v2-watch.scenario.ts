import { type Config } from '../config'
import { type Browser, type Page } from 'puppeteer'
import { Scenario } from './scenario'
import { goto } from '../utils/goto.util'
import { type Bot } from '../bot/Bot'
import { sleep } from '../utils/sleep.util'
import { logAction } from '../utils/log-action.util'
import { clickWhereText } from '../utils/click-where-text.util'

export class OlipV2WatchScenario extends Scenario {
  bot: Bot
  config: Config
  browser: Browser
  page: Page

  async run(): Promise<void> {
    await this.openDashboard()
    await this.waitForDashboardLoaded()
    await sleep(this.config.sleepBetweenActions)
    await this.openCatalog()
    await this.waitForCatalogLoaded()
    await sleep(this.config.sleepBetweenActions)
  }

  async openDashboard(): Promise<void> {
    await logAction(this.bot, 'openDashboard', async () => {
      await goto(this.page, this.config.olipV2Url)
    })
  }

  async waitForDashboardLoaded(): Promise<void> {
    await logAction(this.bot, 'waitForDashboardLoaded', async () => {
      await this.page.waitForNetworkIdle()
    })
  }

  async openCatalog(): Promise<void> {
    await logAction(this.bot, 'openMediacenter', async () => {
      await clickWhereText(this.page, 'span', 'Catalog')
    })
  }

  async waitForCatalogLoaded(): Promise<void> {
    await logAction(this.bot, 'waitForCatalogLoaded', async () => {
      await this.page.waitForNetworkIdle()
    })
  }
}
