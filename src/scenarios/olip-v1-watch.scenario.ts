import { type Config } from '../config'
import { type Browser, type Page } from 'puppeteer'
import { Scenario } from './scenario'
import { goto } from '../utils/goto.util'
import { type Bot } from '../bot/Bot'
import { sleep } from '../utils/sleep.util'
import { focusTab } from '../utils/focus-tab.util'
import { clickWhereText } from '../utils/click-where-text.util'
import { random } from '../utils/random.util'
import { clickSelector } from '../utils/click-selector.util'
import { logAction } from '../utils/log-action.util'
import { downloadFile } from '../utils/download-file.util'

export class OlipV1WatchScenario extends Scenario {
  bot: Bot
  config: Config
  browser: Browser
  page: Page

  async run(): Promise<void> {
    await this.openDashboard()
    await this.waitForDashboardLoaded()
    await sleep(this.config.sleepBetweenActions)
    await this.openMediacenter()
    await this.waitForMediacenterLoaded()
    await sleep(this.config.sleepBetweenActions)
    await this.selectPlaylist()
    await this.waitForPlaylistLoaded()
    await sleep(this.config.sleepBetweenActions)
    await this.selectContent()
    await this.waitForContentLoaded()
    await sleep(this.config.sleepBetweenActions)
    await this.downloadVideo()
    await sleep(this.config.sleepBetweenActions)
    await this.closeMediacenter()
  }

  async openDashboard(): Promise<void> {
    await logAction(this.bot, 'openDashboard', async () => {
      await goto(this.page, this.config.olipV1Url)
    })
  }

  async waitForDashboardLoaded(): Promise<void> {
    await logAction(this.bot, 'waitForDashboardLoaded', async () => {
      await this.page.waitForNetworkIdle()
    })
  }

  async openMediacenter(): Promise<void> {
    await logAction(this.bot, 'openMediacenter', async () => {
      await clickWhereText(this.page, 'div', 'Mediacenter')
    })
  }

  async waitForMediacenterLoaded(): Promise<void> {
    await logAction(
      this.bot,
      'waitForMediacenterLoaded',
      async () => {
        await sleep(500)
        this.page = await focusTab(this.browser, 1)
        await this.page.waitForNetworkIdle()
      },
      500
    )
  }

  async selectPlaylist(): Promise<void> {
    await logAction(this.bot, 'selectPlaylist', async () => {
      const elementNumber = random(
        0,
        this.config.olipV1PlaylistSelection.length - 1
      )
      const element = this.config.olipV1PlaylistSelection[elementNumber]
      const selector = `ul.grid.list-unstyled li.card:nth-child(${element}) a`
      await clickSelector(this.page, selector)
    })
  }

  async waitForPlaylistLoaded(): Promise<void> {
    await logAction(this.bot, 'waitForPlaylistLoaded', async () => {
      await this.page.waitForNetworkIdle()
    })
  }

  async selectContent(): Promise<void> {
    await logAction(this.bot, 'selectContent', async () => {
      const selector = 'ul.grid.list-unstyled li.card:nth-child(1) a'
      await clickSelector(this.page, selector)
    })
  }

  async waitForContentLoaded(): Promise<void> {
    await logAction(this.bot, 'waitForContentLoaded', async () => {
      await this.page.waitForNetworkIdle()
    })
  }

  async downloadVideo(): Promise<void> {
    await logAction(this.bot, 'downloadVideo', async () => {
      const videoElement = await this.page.$('video')
      if (videoElement !== null) {
        const url = await this.page?.evaluate(async () => {
          const sourceElement = document.querySelector('video source')
          if (sourceElement !== null) {
            // @ts-expect-error "src" property exists
            return sourceElement.src
          }
          return null
        })

        if (url !== null) {
          await downloadFile(url, 100)
        }
      }
    })
  }

  async closeMediacenter(): Promise<void> {
    await logAction(this.bot, 'closeMediacenter', async () => {
      await this.page.close()
      this.page = await focusTab(this.browser, 0)
    })
  }
}
