import { type Bot } from '../bot/Bot'
import { type Config } from '../config'
import { type Browser, type Page } from 'puppeteer'

export class Scenario {
  bot: Bot
  config: Config
  browser: Browser
  page: Page

  constructor(bot: Bot) {
    this.bot = bot
    this.config = bot.config
    this.browser = bot.browser
  }

  async init(): Promise<void> {
    const [page] = await this.browser.pages()
    this.page = page
    await this.page.setCacheEnabled(true)
    this.page.setDefaultNavigationTimeout(this.config.defaultTimeout)
    this.page.setDefaultTimeout(this.config.defaultTimeout)
  }

  async run(): Promise<void> {}
}
