import { type Config } from '../config'
import { type Browser, type Page } from 'puppeteer'
import { Scenario } from './scenario'
import { goto } from '../utils/goto.util'
import { type Bot } from '../bot/Bot'
import { logAction } from '../utils/log-action.util'

export class OlipV2SpeedScenario extends Scenario {
  bot: Bot
  config: Config
  browser: Browser
  page: Page
  private themeIds: number[]
  private username: string
  private password: string
  private olipUrl: string

  constructor(bot: Bot) {
    super(bot)
    this.loadConfig()
  }

  async run(): Promise<void> {
    await this.openHomepage()
    await this.openCatalogPage()
    for (const themeId of this.themeIds) {
      await this.openFrontOfficeTheme(themeId)
    }
    await this.login()
    await this.openBackOfficeThemesList()
    for (const themeId of this.themeIds) {
      await this.openBackOfficeTheme(themeId)
    }
    await this.logout()
  }

  async openHomepage(): Promise<void> {
    await logAction(this.bot, 'openHomepage', async () => {
      await goto(this.page, `${this.olipUrl}`)
      await this.page.waitForNetworkIdle()
    })
  }

  async openCatalogPage(): Promise<void> {
    await logAction(this.bot, 'openCatalogPage', async () => {
      await goto(this.page, `${this.olipUrl}/catalog`)
      await this.page.waitForNetworkIdle()
    })
  }

  async openFrontOfficeTheme(themeId: number): Promise<void> {
    await logAction(
      this.bot,
      'openFrontOfficeTheme',
      async () => {
        await goto(this.page, `${this.olipUrl}/category/${themeId}`)
        await this.page.waitForNetworkIdle()
      },
      undefined,
      `themeId: ${themeId}`
    )
  }

  async login(): Promise<void> {
    await logAction(this.bot, 'login', async () => {
      const loginButtonSelector = 'button[data-testid="loginButton"]'
      const submitButtonSelector = 'button[data-testid="submitButton-login"]'
      await this.page.waitForSelector(loginButtonSelector)
      await this.page.click(loginButtonSelector)
      await this.page.waitForSelector('input[id="username"]')
      await this.page.waitForSelector('input[id="password"]')
      await this.page.type('#username', `${this.username}`)
      await this.page.type('#password', `${this.password}`)
      await this.page.waitForSelector(submitButtonSelector)
      await this.page.click(submitButtonSelector)
      await this.page.waitForNetworkIdle()
    })
  }

  async openBackOfficeThemesList(): Promise<void> {
    await logAction(this.bot, 'openBackOfficeThemesList', async () => {
      await goto(this.page, `${this.olipUrl}/settings/contents`)
      await this.page.waitForNetworkIdle()
    })
  }

  async openBackOfficeTheme(themeId: number): Promise<void> {
    await logAction(
      this.bot,
      'openBackOfficeTheme',
      async () => {
        await goto(
          this.page,
          `${this.olipUrl}/settings/contents/theme/${themeId}`
        )
        await this.page.waitForNetworkIdle()
      },
      undefined,
      `themeId: ${themeId}`
    )
  }

  async logout(): Promise<void> {
    await logAction(this.bot, 'logout', async () => {
      const logoutButtonSelector = 'button[data-testid="logoutButton"]'
      await this.page.waitForSelector(logoutButtonSelector)
      await this.page.click(logoutButtonSelector)
      await this.page.waitForNetworkIdle()
    })
  }

  private loadConfig(): void {
    if (process.env.SCENARIO_3_THEME_IDS !== undefined) {
      this.themeIds = process.env.SCENARIO_3_THEME_IDS.split(';').map(
        (themeId) => parseInt(themeId)
      )
    } else {
      this.themeIds = []
    }
    this.username = process.env.SCENARIO_3_USERNAME ?? ''
    this.password = process.env.SCENARIO_3_PASSWORD ?? ''
    this.olipUrl = process.env.SCENARIO_3_OLIP_V2_URL ?? ''
  }
}
