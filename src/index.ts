import { Config } from './config'
import { Bot } from './bot/Bot'
import { type IScenario, scenarios, type ScenariosEnum } from './scenarios'
import { writeFileSync } from 'fs'
import { join } from 'path'
import { stopTest } from './utils/stop-test.util'

async function bootstrap(): Promise<void> {
  const config = new Config()
  const scenarioNumber = config.scenario as ScenariosEnum
  const foundScenario = scenarios.find((s) => s.name === scenarioNumber)
  if (foundScenario === undefined) {
    throw new Error('Scenario not found')
  }
  const scenario = foundScenario as IScenario
  const logsFilename = `${Date.now()}-${config.botsToRun}-${
    scenario.className
  }.csv`
  writeFileSync(
    join(__dirname, '../logs', logsFilename),
    'Date,N° Poste,Total postes,Action,Status,Temps (ms),Commentaire\n'
  )
  for (let i = 0; i < config.botsToRun; i++) {
    const bot = new Bot(config, i + 1, logsFilename)
    await bot.init(scenario)
    bot.run().catch((e) => {
      console.error(e)
    })
  }
  stopTest(config.testDurationInMinutes)
}

bootstrap().catch((e) => {
  console.error(e)
})
