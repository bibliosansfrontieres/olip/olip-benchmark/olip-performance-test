export function random(min: number, max: number): number {
  const randomNumber = Math.random()
  const scaledNumber = min + Math.floor(randomNumber * (max - min + 1))
  return scaledNumber
}
