import { appendFileSync } from 'fs'
import { type Bot } from '../bot/Bot'
import { join } from 'path'
import { formatTimestamp } from './format-timestamp.util'

export function writeLog(
  bot: Bot,
  action: string,
  success: boolean,
  time: number,
  comment: string = ''
): void {
  const date = formatTimestamp(Date.now())
  const csvLine = `${date},${bot.identifier},${
    bot.config.botsToRun
  },${action},${success ? 'OK' : 'KO'},${time},${comment}`
  appendFileSync(
    join(__dirname, '../../logs', bot.logsFilename),
    `${csvLine}\n`
  )
  console.log(
    `[${bot.identifier}/${bot.config.botsToRun}] ${action} ${
      success ? 'OK' : 'KO'
    } - ${time}ms`
  )
}
