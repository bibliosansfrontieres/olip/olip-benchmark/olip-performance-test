import axios from 'axios'
import { createWriteStream, unlinkSync } from 'fs'
import { join } from 'path'

export async function downloadFile(
  url: string,
  maxSizeMB: number = 100
): Promise<void> {
  try {
    const outputPath = join(__dirname, '../../temp', Date.now().toString())

    // Calculate the maximum file size in bytes
    const maxSizeBytes = maxSizeMB * 1024 * 1024 // 1 MB = 1024 * 1024 bytes

    // Get the total content length of the file
    const totalContentLength = (await axios.head(url)).headers['content-length']

    // Determine the byte range to download based on the maximum file size
    const startByte = 0
    const endByte = Math.min(maxSizeBytes - 1, totalContentLength - 1)

    // Download the file within the specified byte range
    const response = await axios.get(url, {
      responseType: 'stream',
      headers: {
        Range: `bytes=${startByte}-${endByte}`
      }
    })

    const partialOutputPath = outputPath + '.partial'

    const writeStream = createWriteStream(partialOutputPath)

    response.data.pipe(writeStream)

    await new Promise<void>((resolve, reject) => {
      writeStream.on('finish', () => {
        resolve()
      })

      writeStream.on('error', (err: Error) => {
        reject(err)
      })
    })

    unlinkSync(partialOutputPath)
  } catch (error) {
    console.error(error)
    throw new Error('could not download file')
  }
}
