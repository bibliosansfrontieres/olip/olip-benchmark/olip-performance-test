import { type Page } from 'puppeteer'

export async function clickSelector(
  page: Page,
  selector: string
): Promise<void> {
  const element = await page.waitForSelector(selector)
  if (element !== null) {
    await element.click()
  } else {
    await clickSelector(page, selector)
  }
}
