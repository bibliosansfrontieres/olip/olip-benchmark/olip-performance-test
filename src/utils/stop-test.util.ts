export function stopTest(minutes: number): void {
  const milliseconds = minutes * 60 * 1000
  setTimeout(() => {
    console.log(`Program has been running for ${minutes} minutes. Exiting...`)
    process.exit(0)
  }, milliseconds)
}
