import { type Browser, type Page } from 'puppeteer'

export async function focusTab(
  browser: Browser,
  tabIndex: number
): Promise<Page> {
  const pages = await browser.pages()
  const page = pages[tabIndex]
  if (page === undefined) {
    throw new Error(`Tab index ${tabIndex} not found`)
  }
  await page.setCacheEnabled(false)
  return page
}
