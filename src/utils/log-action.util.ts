import { type Bot } from '../bot/Bot'
import { writeLog } from './log.util'

export async function logAction(
  bot: Bot,
  action: string,
  callback: () => Promise<any>,
  timeCorrection: number = 0,
  comment?: string
): Promise<any> {
  const timeBeforeAction = Date.now()
  try {
    const callbackResult = await callback()
    const timeAfterAction = Date.now()
    const totalTimeAction = timeAfterAction - timeBeforeAction - timeCorrection
    writeLog(bot, action, true, totalTimeAction, comment)
    return callbackResult
  } catch (e: any) {
    const timeAfterCrash = Date.now()
    const totalTimeAction = timeAfterCrash - timeBeforeAction
    console.error(e)
    if (e.message !== undefined && comment === undefined) {
      comment = e.message
    } else if (e.message !== undefined) {
      comment = comment + ' - ' + e.message
    }
    writeLog(bot, action, false, totalTimeAction, comment)
    throw new Error()
  }
}
