export function formatTimestamp(timestamp: number): string {
  const date = new Date(timestamp) // Convert timestamp to milliseconds
  const day = String(date.getDate()).padStart(2, '0') // Add leading zero if necessary
  const month = String(date.getMonth() + 1).padStart(2, '0') // Add leading zero if necessary
  const year = date.getFullYear()
  const hours = String(date.getHours()).padStart(2, '0') // Add leading zero if necessary
  const minutes = String(date.getMinutes()).padStart(2, '0') // Add leading zero if necessary
  const seconds = String(date.getSeconds()).padStart(2, '0') // Add leading zero if necessary

  const formattedDate = `${day}/${month}/${year}`
  const formattedTime = `${hours}:${minutes}:${seconds}`

  return `${formattedDate} ${formattedTime}`
}
