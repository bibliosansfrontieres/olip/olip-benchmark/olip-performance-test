import { type Page } from 'puppeteer'

/**
 * Navigates the given page to the specified URL and measures the time it takes to load.
 *
 * @param {Page} page - The page object that will be navigated.
 * @param {string} url - The URL to navigate to.
 * @return {Promise<number>} The time it took in milliseconds for the page to load.
 */
export async function goto(
  page: Page,
  url: string,
  timeout: number = 30000
): Promise<void> {
  await page.goto(url, {
    timeout
  })
}
