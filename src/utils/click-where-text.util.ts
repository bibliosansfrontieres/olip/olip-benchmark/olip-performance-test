import { type ElementHandle, type Page } from 'puppeteer'

export async function clickWhereText(
  page: Page,
  htmlElement: string,
  text: string
): Promise<void> {
  const elements = await page.$x(
    `//${htmlElement}[contains(text(), '${text}')]`
  )
  const element = elements[0]
  if (element !== undefined) {
    await (element as ElementHandle<Element>).click()
  } else {
    await clickWhereText(page, htmlElement, text)
  }
}
