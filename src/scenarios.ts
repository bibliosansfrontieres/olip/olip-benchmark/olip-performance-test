export enum ScenariosEnum {
  OLIP_V1_WATCH = '1',
  OLIP_V2_WATCH = '2',
  OLIP_V2_SPEED = '3'
}

export interface IScenario {
  name: ScenariosEnum
  filename: string
  className: string
}

export const scenarios = [
  {
    name: ScenariosEnum.OLIP_V1_WATCH,
    filename: 'olip-v1-watch.scenario',
    className: 'OlipV1WatchScenario'
  },
  {
    name: ScenariosEnum.OLIP_V2_WATCH,
    filename: 'olip-v2-watch.scenario',
    className: 'OlipV2WatchScenario'
  },
  {
    name: ScenariosEnum.OLIP_V2_SPEED,
    filename: 'olip-v2-speed.scenario',
    className: 'OlipV2SpeedScenario'
  }
]
